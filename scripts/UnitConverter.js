const button = document.getElementById("weather-condition-data-main");
const currentTempText = document.getElementById("current-temp-data");
const currentTempUnit = document.getElementById("temp-unit");

const units = ["C", "F"];
var currentUnit = 0;

button.addEventListener("click", () => {
  if (currentUnit == 0) {
    currentUnit = 1;
    currentTempText.innerText = calciusToFahrenheit(
      currentTempText.textContent
    ).toFixed(2);
    currentTempUnit.innerText = units[currentUnit];
  } else {
    currentUnit = 0;
    currentTempText.innerText = fahrenheitToCalcius(
      currentTempText.textContent
    ).toFixed(2);
    currentTempUnit.innerText = units[currentUnit];
  }
});

function calciusToFahrenheit(temp) {
  return temp * 1.8 + 32;
}

function fahrenheitToCalcius(temp) {
  return ((temp - 32) * 5) / 9;
}
