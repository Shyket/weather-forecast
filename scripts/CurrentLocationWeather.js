var apiKey = "a86dbbd269de19bbc553bf241559905b";

function initialize() {
  if (window.isSecureContext) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          fetchWeatherDataForCurrentLocation(
            position.coords.latitude,
            position.coords.longitude
          ).then((res) => {
            displayWeatherData(res);
          });
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      console.error("geolocation error");
    }
  } else {
    console.error("connection not secure");
  }
}

async function fetchWeatherDataForCurrentLocation(latitude, longitude) {
  const data = {
    lat: latitude,
    lon: longitude,
    appid: apiKey,
    units: "metric",
  };
  return fetch(
    "http://api.openweathermap.org/data/2.5/weather?" +
      new URLSearchParams(data).toString(),
    {
      mode: "cors",
    }
  ).then((response) => {
    return response.json();
  });
}

function setWeatherIcon(iconID) {
  let iconURL = "http://openweathermap.org/img/wn/" + iconID + "@2x.png";
  document.getElementById(
    "weather-condition-icon"
  ).innerHTML = `<img src=${iconURL} width='400px' height='150px'>`;
}

function displayWeatherData(data) {
  setWeatherIcon(data.weather[0].icon);
  document.getElementById("current-location-name").innerText = data.name;
  document.getElementById("latitude").innerText = data.coord.lat;
  document.getElementById("langitude").innerText = data.coord.lon;

  var date = new Date(data.dt * 1000);
  

  document.getElementById("current-date").innerText =
    date.getDate() + " / " + date.getMonth() + " / " + date.getFullYear();
  document.getElementById("current-temp-data").innerText =
    data.main.temp.toFixed(2);
  document.getElementById("weather-description").innerText =
    data.weather[0].description;

  document.getElementById("max-temp").innerText = data.main.temp_max.toFixed(2);
  document.getElementById("min-temp").innerText = data.main.temp_min.toFixed(2);
  document.getElementById("temp-feels").innerText =
    data.main.feels_like.toFixed(2);

  document.getElementById("condition").innerText = data.weather[0].description;
  document.getElementById("cloudiness").innerText = data.clouds.all;
  document.getElementById("humidity").innerText = data.main.humidity;
  document.getElementById("pressure").innerText = data.main.pressure;
  document.getElementById("wind-speed").innerText = data.wind.speed;
  document.getElementById("wind-deg").innerText = data.wind.deg;
}

initialize();
