var apiKey = "a86dbbd269de19bbc553bf241559905b";

const suggestionList = document.getElementById("suggestion-list");
//const cityInputField = document.getElementById("search-input");

suggestionList.addEventListener("click", (e) => {
  if (e.target.tagName === "LI") {
    var city = e.target.innerText;
    cityInputField.value = e.target.innerText;
    document.getElementById(divs[currentDiv]).style.display = "flex";
    fetchWeatherDataByCityName(city).then((res) => {
      suggestionList.innerHTML = "";
      displayCityWeatherData(res);
    });
  }
});

async function fetchWeatherDataByCityName(cityName) {
  const data = {
    q: cityName,
    appid: apiKey,
    cnt: 1,
    units: "metric",
  };
  return fetch(
    "http://api.openweathermap.org/data/2.5/weather?" +
      new URLSearchParams(data).toString(),
    {
      mode: "cors",
    }
  ).then((response) => {
    return response.json();
  });
}

function setCityWeatherIcon(iconID) {
  let iconURL = "http://openweathermap.org/img/wn/" + iconID + "@2x.png";
  document.getElementById(
    "city-weather-icon"
  ).innerHTML = `<img src=${iconURL} width='400px' height='150px'>`;
}

function displayCityWeatherData(data) {
  console.log(data);
  setCityWeatherIcon(data.weather[0].icon);

  document.getElementById("city-name").innerText = data.name;
  document.getElementById(
    "city-temp"
  ).innerHTML = `</span>${data.main.temp.toFixed(2)}<sup>&deg</sup>&nbspC`;
  document.getElementById("city-max-temp").innerText =
    data.main.temp_max.toFixed(2);
  document.getElementById("city-min-temp").innerText =
    data.main.temp_min.toFixed(2);
  document.getElementById("city-temp-feels").innerText =
    data.main.feels_like.toFixed(2);

  document.getElementById("city-condition").innerText =
    data.weather[0].description;
  document.getElementById("city-cloudiness").innerText = data.clouds.all;
  document.getElementById("city-humidity").innerText = data.main.humidity;
  document.getElementById("city-pressure").innerText = data.main.pressure;
  document.getElementById("city-wind-speed").innerText = data.wind.speed;
  document.getElementById("city-wind-deg").innerText = data.wind.deg;
}

var divs = ["city-temp-status", "city-weather-status", "city-wind-status"];
var currentDiv = 0;

document
  .getElementById("city-weather-container")
  .addEventListener("click", () => {
    document.getElementById(divs[currentDiv]).style.display = "none";

    if (currentDiv == divs.length - 1) {
      currentDiv = 0;
    } else {
      currentDiv++;
    }
    document.getElementById(divs[currentDiv]).style.display = "flex";
  });
