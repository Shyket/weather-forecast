var apiKey = "738ba351e38db93b2d20147b4beae02c";

if (window.isSecureContext) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        fetchForecastDataForCurrentLocation(
          position.coords.latitude,
          position.coords.longitude
        ).then((res) => {
          displayForecastData(res);
        });
      },
      (error) => {
        console.error(error);
      }
    );
  } else {
    console.error("geolocation error");
  }
} else {
  console.error("connection not secure");
}

async function fetchForecastDataForCurrentLocation(latitude, longitude) {
  const data = {
    lat: latitude,
    lon: longitude,
    appid: "a86dbbd269de19bbc553bf241559905b",
    units: "metric",
  };
  return fetch(
    "http://api.openweathermap.org/data/2.5/forecast?" +
      new URLSearchParams(data).toString(),
    {
      mode: "cors",
    }
  ).then((response) => {
    return response.json();
  });
}



function displayForecastData(data) {
  const forecastList = document.getElementById("forecast-list");
  forecastList.innerHTML = "";
  console.log(data);
  let months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  let days = [
    "Saturday",
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
  ];

  for (i = 0; i < data.list.length; i++) {
    var li = document.createElement("li");
    var date = new Date(data.list[i].dt * 1000);
    var dateString =
      date.getHours() +
      ":" +
      date.getMinutes() +
      "   " +
      days[date.getDay()] +
      ", " +
      date.getDate() +
      " " +
      months[date.getMonth()];
    let iconURL =
      "http://openweathermap.org/img/wn/" +
      data.list[0].weather[0].icon +
      "@2x.png";

    li.innerHTML = `<div id="forcast-date-div">
                      <p><span id="forecast-date">${dateString}</span></p>
                    </div>
                    <div id="forecast-weather-data">
                      <div id="forecast-weather-icon">
                        <img src=${iconURL} width='50px' height='50px'>
                      </div>

                    
                      <div id="forecast-data">
                        <p><span id="forecast-temp">${data.list[i].main.temp}</span>&nbsp<sup>&deg</sup>C</p>
                        <p><span id="forecast-condition">${data.list[i].weather[0].description}</span>&nbsp<sup>
                      </div>
                    </div>`;
    forecastList.appendChild(li);
  }
}
