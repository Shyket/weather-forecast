# Weather Forecast


# Requirements
- show weather information of the current location of user (max & min temparature, humidity, wind speed, cloud)
- show temparature both in calcius and fahrenheit unit
- show current location name of the user
- show list of countries from where user can choose a city to see it's weather information
- show weather icon based on weather condition

